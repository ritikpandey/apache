variable "vpc_id" {
  default     = ""
  type        = string
  description = "VPC ID for apache vpc"
}
variable "vpc_cidr" {
  default = ""
}
variable "vpc_tags" {
  default     = {}
  type        = map(string)
  description = "tags for apache vpc"
}
variable "public_subnet_cidr" {
  type        = string
  description = "CIDR block for subnet"
  default     = ""
}
variable "public_subnet_tags" {
  default     = {}
  description = "tags for apache subnet"
  type        = map(string)
}
variable "public_subnet_zone" {
  default = {}
}
variable "sg_pb" {
  type        = string
  description = "public security group"
  default     = "Apache-public-sg"
}
variable "https_port" {
  type        = string
  description = "TCP port for https"
  default     = "443"
}
variable "ssh_port" {
  type        = string
  description = "TCP port for ssh"
  default     = "22"
}

variable "pb_sg_tags" {
  default     = {}
  description = "Public Security group tags for Apache"
  type        = map(string)
}

variable "public_routeTable_tags" {
  description = "tags for apache vpc"
  default = {
    Name    = "pub_RT",
    Owner   = "Ritik",
    purpose = "apache__pub_RT "

  }
}

variable "igw_tags" {
  description = "tag for internet gateway of Sonar-vpc"
  default = {
    Name  = "Apache-igw"
    Owner = "Ritik"
  }
}
variable "ami" {
  type        = string
  description = "ami id for sonarqube instances"
  default     = ""
}
variable "apache_tag" {
  type        = map(string)
  description = "tags for sonarqube master node instance"
  default     = {}
}



