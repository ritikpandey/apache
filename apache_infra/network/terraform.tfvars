vpc_cidr = "11.0.0.0/16"
vpc_tags = {
  Name    = "My-vpc",
  Owner   = "Ritik",
  purpose = "for apache infra "
}
public_subnet_cidr = "11.0.192.0/19"
public_subnet_tags = {
  Name  = "Apache_pub_subnet"
  Owner = "Ritik"
}

pb_sg_tags = {
  Name = "public-sg"
}
public_subnet_zone = "ap-south-1b"
ami                = "ami-0e6329e222e662a52"
master_tag = {
  Name  = "apache_web",
  Owner = "ritik",
}
