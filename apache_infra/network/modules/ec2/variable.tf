variable "ami" {
  type        = string
  default     = ""
}
variable "ec2_tags" {
  type        = map(string)
  default = {
    Name    = "apache-ec2",
    Owner   = "Ritik",
  }
}
variable "security_group" {
  type        = string
  default     = ""
}
variable "subnet" {
  type        = string
  default     = ""
}
