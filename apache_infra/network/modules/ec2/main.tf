resource "aws_instance" "ec2" {
  ami                    = var.ami
  instance_type          = "t2.large"
  tags                   = var.ec2_tags
  key_name               = "apache-key"
  vpc_security_group_ids = [var.security_group]
  subnet_id              = var.subnet
 user_data = <<EOF
#!/bin/bash
sudo yum update -y
sudo yum install httpd.x86_64 -y
sudo systemctl start httpd
sudo systemctl enable httpd
sudo echo '<!DOCTYPE html>' > /var/www/html/index.html
sudo echo '<html lang="en">' >> /var/www/html/index.html
sudo echo '<body style="background-color:black;">' >> /var/www/html/index.html
sudo echo '  <h1 style="color:red;">Week 20 Terraform Project: HTTP access successfully configured!</h1>' >> /var/www/html/index.html
sudo echo '</body>' >> /var/www/html/index.html
sudo echo '</html>' >> /var/www/html/index.html
EOF
}


